﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{

    [Table(name: "TProfessor")]
    public class Professor : Tabela
    {

        [Display(Name = "USUÁRIO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string UsuarioID { get; set; }
 
    }
}