﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SemedInstitucional.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Código")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Lembrar deste navegador?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [Display(Name = "Lembrar-me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O/A {0} deve ter no mínimo {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Senha")]
        [Compare("Password", ErrorMessage = "A senha e a senha de confirmação não correspondem.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "NOME")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [Display(Name = "SETOR")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public int Setor { get; set; }

        [Display(Name = "MATRÍCULA")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Matricula { get; set; }

        [Display(Name = "CARGO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public int CargoID { get; set; }

        [Display(Name = "DATA DE NASCIMENTO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public DateTime DataNascimento { get; set; }

        [Display(Name = "SEXO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public bool Sexo { get; set; }

        [Display(Name = "CPF")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string CPF { get; set; }

    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O/A {0} deve ter no mínimo {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar senha")]
        [Compare("Password", ErrorMessage = "A senha e a senha de confirmação não coincidem.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
    }
}
