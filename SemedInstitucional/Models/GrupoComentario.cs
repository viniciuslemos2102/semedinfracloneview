﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{

    [Table(name: "TGrupoComentario")]
    public class GrupoComentario : Tabela
    {

        [Display(Name = "GRUPO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        [ForeignKey(name: "Grupo")]
        public int GrupoID { get; set; }

        [Display(Name = "MENSAGEM")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Mensagem { get; set; }

        public virtual Grupo Grupo { get; set; }

    }
}