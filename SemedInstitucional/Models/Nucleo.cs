﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{

    [Table(name: "TNucleo")]
    public class Nucleo : Tabela
    {

        [Display(Name = "NOME DO NÚCLEO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Nome { get; set; }

        public virtual ICollection<Escola> Escolas { get; set; }

    }

}