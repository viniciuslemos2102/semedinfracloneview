﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{

    [Table(name: "TGrupoSetor")]
    public class GrupoSetor : Tabela
    {

        [Display(Name = "GRUPO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        [ForeignKey(name: "Grupo")]
        public int GrupoID { get; set; }

        [Display(Name = "SETOR")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        [ForeignKey(name: "Setor")]
        public int SetorID { get; set; }

        public virtual Grupo Grupo { get; set; }

        public virtual Setor Setor { get; set; }

    }
}