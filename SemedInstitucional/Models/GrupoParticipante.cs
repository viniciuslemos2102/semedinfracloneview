﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{

    [Table(name: "TGrupoParticipante")]
    public class GrupoParticipante : Tabela
    {

        [Display(Name = "GRUPO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        [ForeignKey(name: "Grupo")]
        public int GrupoID { get; set; }

        [Display(Name = "PARTICIPANTE")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        [ForeignKey(name: "Participante")]
        public string UsuarioID { get; set; }

        public virtual Grupo Grupo { get; set; }

        public IdentityUser Participante { get; set; }

    }
}