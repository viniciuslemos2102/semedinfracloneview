﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models.Utilidades
{
    public class SystemMessage
    {
        public const string FIELD_REQUIRED = "O campo {0} é obrigatório!";

        public const string ERROR_MESSAGE_FOREIGN_KEY = "Chave estrangeira não encontrada!";

        public const string PERSON = "PERSON_";

        public const string ERROR_ACTION = "Ocorreu um erro no processamento da requisição!";

        public const string SUCCESS_CREATE = "Cadastro realizado com sucesso!";

        public const string SUCCESS_EDIT = "Edição realizada com sucesso!";

        public const string SUCCESS_DELETE = "Exclusão realizada com sucesso!";

        public const string ERROR_CREATE = "Ocorreu um erro ao realizar o cadastro do registro!";

        public const string ERROR_EDIT = "Ocorreu um erro ao realizar a edição do registro!";

        public const string ERROR_DELETE = "Ocorreu um erro ao realizar a exclusão do registro!";

        public const string ERROR_DETAILS = "Ocorreu um erro ao realizar o detalhamento do registro!";

        public const string SUCCESS_RECEBIMENTO = "Recebimento realizado com sucesso!";

        public const string SUCCESS_ENVIO = "Envio realizado com sucesso!";

    }
}