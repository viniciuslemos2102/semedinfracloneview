﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{
    public abstract class Tabela
    {

        [Key]
        public int ID { get; set; }

        [Display(Name = "DELETADO")]
        [ScaffoldColumn(scaffold: false)]
        [DataType(DataType.DateTime)]
        public bool Deletado { get; set; }

        [Display(Name = "DATA DO CADASTRO")]
        [ScaffoldColumn(scaffold: false)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}", ConvertEmptyStringToNull = true)]
        [DataType(DataType.DateTime)]
        public DateTime DataCadastro { get; set; }

        [Display(Name = "DATA DA EXCLUSÃO")]
        [ScaffoldColumn(scaffold: false)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}", ConvertEmptyStringToNull = true)]
        [DataType(DataType.DateTime)]
        public DateTime? DataExclusao { get; set; }

        [Display(Name = "RESPONSÁVEL PELO CADASTRO")]
        [ScaffoldColumn(scaffold: false)]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string CadastroLogID { get; set; }

        [Display(Name = "RESPONSÁVEL PELA EXCLUSÃO")]
        [ScaffoldColumn(scaffold: false)]
        public string ExclusaoLogID { get; set; }

        public void CreateRegister(string Log)
        {
            this.CadastroLogID = Log;
            this.DataCadastro = DateTime.Now;
        }

        public void DeleteRegister(string Log)
        {
            this.ExclusaoLogID = Log;
            this.DataExclusao = DateTime.Now;
            this.Deletado = true;
        }


    }
}