﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{

    [Table(name: "TGrupo")]
    public class Grupo : Tabela
    {
        [Display(Name = "ASSUNTO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Assunto { get; set; }

        [Display(Name = "DESCRIÇÃO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        [DataType(DataType.MultilineText)]
        public string Descricao { get; set; }

        public virtual ICollection<GrupoComentario> GrupoComentarios { get; set; }

        public virtual ICollection<GrupoParticipante> GrupoParticipantes { get; set; }

        public virtual ICollection<GrupoSetor> GrupoSetores { get; set; }

    }
}