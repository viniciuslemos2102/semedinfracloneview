﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SemedInstitucional.Models
{
    // É possível adicionar dados do perfil do usuário adicionando mais propriedades na sua classe ApplicationUser, visite https://go.microsoft.com/fwlink/?LinkID=317594 para obter mais informações.
    public class ApplicationUser : IdentityUser
    {

        [Display(Name = "NOME")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [Display(Name = "SETOR")]
        [ForeignKey(name: "Setor")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public int SetorID { get; set; }

        [Display(Name = "MATRÍCULA")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Matricula { get; set; }

        [Display(Name = "CARGO")]
        [ForeignKey(name: "Cargo")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public int CargoID { get; set; }

        [Display(Name = "DATA DE NASCIMENTO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public DateTime DataNascimento { get; set; }

        [Display(Name = "SEXO")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public bool Sexo { get; set; }

        [Display(Name = "CPF")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string CPF { get; set; }

        public virtual Setor Setor { get; set; }

        public virtual Cargo Cargo { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Observe que o authenticationType deve corresponder àquele definido em CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("Nome", this.Nome));
            // Adicionar declarações de usuário personalizado aqui
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("SemedInstitucionalDB", throwIfV1Schema: false)
        {
        }

        public DbSet<Setor> Setores { get; set; }

        public DbSet<Cargo> Cargos { get; set; }

        public DbSet<Escola> Escolas { get; set; }

        public DbSet<Nucleo> Nucleos { get; set; }

        public DbSet<Grupo> Grupos { get; set; }

        public DbSet<GrupoSetor> GrupoSetores { get; set; }

        public DbSet<GrupoParticipante> GrupoParticipantes { get; set; }

        public DbSet<GrupoComentario> GrupoComentarios { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}