﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemedInstitucional.Models
{

    [Table(name: "TEscola")]
    public class Escola : Tabela
    {

        [Display(Name = "NOME DA ESCOLA")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [Display(Name = "NÚCLEO")]
        [ForeignKey(name: "Nucleo")]
        [Required(ErrorMessage = "O CAMPO É {0} OBRIGATÓRIO!", AllowEmptyStrings = false)]
        public int NucleoID { get; set; }

        public virtual Nucleo Nucleo { get; set; }

    }

}