﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SemedInstitucional.Startup))]
namespace SemedInstitucional
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
