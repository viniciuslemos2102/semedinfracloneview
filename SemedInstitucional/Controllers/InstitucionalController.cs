﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SemedInstitucional.Controllers
{
    public class InstitucionalController : Controller
    {
        public ViewResult Home()
        {
            return View();
        }

        protected override void HandleUnknownAction(string actionName)
        {
            this.View(actionName).ExecuteResult(this.ControllerContext);
        }

    }
}