﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SemedInstitucional.Models.Utilidades;
using Microsoft.AspNet.Identity.Owin;

using SemedInstitucional.Models;

namespace SemedInstitucional.Controllers
{
    public class GruposController : Controller
    {
        private ApplicationDbContext _db;

        public GruposController()
        {
        }

        public GruposController(ApplicationDbContext db)
        {
            _db = db;
        }

        public ApplicationDbContext db {
            get {
                return _db ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set {
                _db = value;
            }
        }

        // GET: Grupos
        public async Task<ActionResult> Index()
        {
            try
            {
                return View(await db.Grupos.Where(x => x.Deletado == false).ToListAsync());
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_ACTION + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Grupos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Grupo grupo = await db.Grupos.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (grupo == null)
                {
                    return HttpNotFound();
                }
                return View(grupo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DETAILS + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Grupos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Grupos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Grupo grupo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    grupo.CreateRegister("Teste");
                    db.Grupos.Add(grupo);
                    await db.SaveChangesAsync();
                    TempData["Mensagem"] = SystemMessage.SUCCESS_CREATE;
                    return RedirectToAction("Index");
                }

                return View(grupo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_CREATE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Grupos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Grupo grupo = await db.Grupos.FindAsync(id);
                if (grupo == null)
                {
                    return HttpNotFound();
                }
                return View(grupo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Grupos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Grupo grupo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Grupo obj = db.Grupos.Find(grupo.ID);
                    obj = grupo;
                    db.Entry(obj).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    TempData["Mensagem"] = SystemMessage.SUCCESS_EDIT;
                    return RedirectToAction("Index");
                }
                return View(grupo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Grupos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Grupo grupo = await db.Grupos.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (grupo == null)
                {
                    return HttpNotFound();
                }
                return View(grupo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Grupos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Grupo grupo = await db.Grupos.FindAsync(id);
                grupo.DeleteRegister("");
                db.Entry(grupo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_DELETE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        public async Task<JsonResult> GetList(string search)
        {
            var obj = await db.Grupos
                    .Where(x => x.ID.ToString().Contains(search) && x.Deletado == false)
                    .Select(x => new { id = x.ID, text = x.ID })
                    .ToListAsync();
            return Json(new { items = obj }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
