﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SemedInstitucional.Models.Utilidades;
using Microsoft.AspNet.Identity.Owin;

using SemedInstitucional.Models;

namespace SemedInstitucional.Controllers
{
    public class CargosController : Controller
    {
        private ApplicationDbContext _db;

        public CargosController()
        {
        }

        public CargosController(ApplicationDbContext db)
        {
            _db = db;
        }

        public ApplicationDbContext db {
            get {
                return _db ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set {
                _db = value;
            }
        }

        // GET: Cargos
        public async Task<ActionResult> Index()
        {
            try
            {

                return View(await db.Cargos.Where(x => x.Deletado == false).ToListAsync());
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_ACTION + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Cargos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cargo cargo = await db.Cargos.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (cargo == null)
                {
                    return HttpNotFound();
                }
                return View(cargo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DETAILS + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Cargos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cargos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Cargo cargo)
        {
            try
            {
                cargo.CreateRegister("Teste");
                db.Cargos.Add(cargo);
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_CREATE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_CREATE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Cargos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cargo cargo = await db.Cargos.FindAsync(id);
                if (cargo == null)
                {
                    return HttpNotFound();
                }
                return View(cargo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Cargos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Cargo cargo)
        {
            try
            {
                Cargo obj = db.Cargos.Find(cargo.ID);
                obj.Nome = cargo.Nome;
                db.Entry(obj).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_EDIT;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Cargos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cargo cargo = await db.Cargos.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (cargo == null)
                {
                    return HttpNotFound();
                }
                return View(cargo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Cargos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Cargo cargo = await db.Cargos.FindAsync(id);
                cargo.DeleteRegister("dsadsa");
                db.Entry(cargo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_DELETE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        public async Task<JsonResult> GetList(string search)
        {
            var obj = await db.Cargos
                    .Where(x => x.ID.ToString().Contains(search) && x.Deletado == false)
                    .Select(x => new { id = x.ID, text = x.ID })
                    .ToListAsync();
            return Json(new { items = obj }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
