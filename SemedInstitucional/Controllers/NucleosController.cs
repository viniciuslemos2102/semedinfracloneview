﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SemedInstitucional.Models.Utilidades;
using SemedInstitucional.Models;
using Microsoft.AspNet.Identity.Owin;

namespace SemedInstitucional.Controllers
{
    public class NucleosController : Controller
    {
        private ApplicationDbContext _db;

        public NucleosController()
        {
        }

        public NucleosController(ApplicationDbContext db)
        {
            _db = db;
        }

        public ApplicationDbContext db {
            get {
                return _db ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set {
                _db = value;
            }
        }

        // GET: Nucleos
        public async Task<ActionResult> Index()
        {
            try
            {
                return View(await db.Nucleos.Where(x => x.Deletado == false).ToListAsync());
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_ACTION + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Nucleos/Details/5
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Nucleo nucleo = await db.Nucleos.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (nucleo == null)
                {
                    return HttpNotFound();
                }
                return View(nucleo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DETAILS + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Nucleos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Nucleos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Nucleo nucleo)
        {
            try
            {
                nucleo.CreateRegister("Teste");
                db.Nucleos.Add(nucleo);
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_CREATE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_CREATE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Nucleos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Nucleo nucleo = await db.Nucleos.FindAsync(id);
                if (nucleo == null)
                {
                    return HttpNotFound();
                }
                return View(nucleo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Nucleos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Nucleo nucleo)
        {
            try
            {
                Nucleo obj = db.Nucleos.Find(nucleo.ID);
                obj.Nome = nucleo.Nome;
                db.Entry(obj).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_EDIT;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Nucleos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Nucleo nucleo = await db.Nucleos.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (nucleo == null)
                {
                    return HttpNotFound();
                }
                return View(nucleo);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Nucleos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Nucleo nucleo = await db.Nucleos.FindAsync(id);
                nucleo.DeleteRegister("Teste");
                db.Entry(nucleo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_DELETE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        public async Task<JsonResult> GetList(string search)
        {
            var obj = await db.Nucleos
                    .Where(x => x.ID.ToString().Contains(search) && x.Deletado == false)
                    .Select(x => new { id = x.ID, text = x.ID })
                    .ToListAsync();
            return Json(new { items = obj }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
