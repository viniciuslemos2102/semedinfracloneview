﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SemedInstitucional.Models.Utilidades;
using Microsoft.AspNet.Identity.Owin;

using SemedInstitucional.Models;

namespace SemedInstitucional.Controllers
{
    public class EscolasController : Controller
    {
        private ApplicationDbContext _db;

        public EscolasController()
        {
        }

        public EscolasController(ApplicationDbContext db)
        {
            _db = db;
        }

        public ApplicationDbContext db {
            get {
                return _db ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set {
                _db = value;
            }
        }

        public async Task<ActionResult> RedesEnsino()
        {
            try
            {
                List<IGrouping<Nucleo,Escola>> escolas = await db.Escolas
                    .Where(x => x.Deletado == false).GroupBy(y => y.Nucleo).ToListAsync();
                return View(escolas);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_ACTION + release.Message;
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: Escolas
        public async Task<ActionResult> Index()
        {
            try
            {

                var escolas = db.Escolas.Where(x => x.Deletado == false).Include(e => e.Nucleo);
                return View(await escolas.ToListAsync());

            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_ACTION + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Escolas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Escola escola = await db.Escolas.Where(x => x.ID == id).Include(e => e.Nucleo).FirstOrDefaultAsync();
                if (escola == null)
                {
                    return HttpNotFound();
                }
                return View(escola);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DETAILS + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Escolas/Create
        public ActionResult Create()
        {
            ViewBag.NucleoID = new SelectList(db.Nucleos.Where(x => x.Deletado == false), "ID", "Nome");
            return View();
        }

        // POST: Escolas/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Escola escola)
        {
            try
            {
                escola.CreateRegister("Teste");
                db.Escolas.Add(escola);
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_CREATE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_CREATE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Escolas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Escola escola = await db.Escolas.FindAsync(id);
                if (escola == null)
                {
                    return HttpNotFound();
                }
                ViewBag.NucleoID = new SelectList(db.Nucleos.Where(x => x.Deletado == false), "ID", "Nome", escola.NucleoID);
                return View(escola);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Escolas/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Escola escola)
        {
            try
            {
                Escola obj = db.Escolas.Find(escola.ID);
                obj.Nome = escola.Nome;
                obj.NucleoID = escola.NucleoID;
                db.Entry(obj).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_EDIT;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Escolas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Escola escola = await db.Escolas.Where(x => x.ID == id).Include(e => e.Nucleo).FirstOrDefaultAsync();
                if (escola == null)
                {
                    return HttpNotFound();
                }
                return View(escola);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Escolas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Escola escola = await db.Escolas.FindAsync(id);
                escola.DeleteRegister("");
                db.Entry(escola).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_DELETE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        public async Task<JsonResult> GetList(string search)
        {
            var obj = await db.Escolas
                    .Where(x => x.ID.ToString().Contains(search) && x.Deletado == false)
                    .Select(x => new { id = x.ID, text = x.ID })
                    .ToListAsync();
            return Json(new { items = obj }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
