﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SemedInstitucional.Models.Utilidades;
using Microsoft.AspNet.Identity.Owin;

using SemedInstitucional.Models;

namespace SemedInstitucional.Controllers
{
    public class SetoresController : Controller
    {
        private ApplicationDbContext _db;

        public SetoresController()
        {
        }

        public SetoresController(ApplicationDbContext db)
        {
            _db = db;
        }

        public ApplicationDbContext db {
            get {
                return _db ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set {
                _db = value;
            }
        }

        // GET: Setores
        public async Task<ActionResult> Index()
        {
            try
            {

                return View(await db.Setores.Where(x => x.Deletado == false).ToListAsync());
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_ACTION + release.Message;
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> SetoresInternos()
        {
            try
            {
                List<Setor> setores = await db.Setores
                    .Where(x => x.Deletado == false).ToListAsync();
                return View(setores);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_ACTION + release.Message;
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: Setores/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Setor setor = await db.Setores.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (setor == null)
                {
                    return HttpNotFound();
                }
                return View(setor);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DETAILS + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Setores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Setores/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Setor setor)
        {
            try
            {
                setor.CreateRegister("Teste");
                db.Setores.Add(setor);
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_CREATE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_CREATE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Setores/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Setor setor = await db.Setores.FindAsync(id);
                if (setor == null)
                {
                    return HttpNotFound();
                }
                return View(setor);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Setores/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Setor setor)
        {
            try
            {
                Setor obj = db.Setores.Find(setor.ID);
                obj.Nome = setor.Nome;
                db.Entry(obj).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_EDIT;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_EDIT + release.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Setores/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Setor setor = await db.Setores.Where(x => x.ID == id).FirstOrDefaultAsync();
                if (setor == null)
                {
                    return HttpNotFound();
                }
                return View(setor);
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Setores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Setor setor = await db.Setores.FindAsync(id);
                setor.DeleteRegister("");
                db.Entry(setor).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Mensagem"] = SystemMessage.SUCCESS_DELETE;
                return RedirectToAction("Index");
            }
            catch (Exception release)
            {
                TempData["Mensagem"] = SystemMessage.ERROR_DELETE + release.Message;
                return RedirectToAction("Index");
            }
        }

        public async Task<JsonResult> GetList(string search)
        {
            var obj = await db.Setores
                    .Where(x => x.ID.ToString().Contains(search) && x.Deletado == false)
                    .Select(x => new { id = x.ID, text = x.ID })
                    .ToListAsync();
            return Json(new { items = obj }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
